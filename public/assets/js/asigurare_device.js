$(document).ready(function() {
    var sliders =  $(".services, .did-you-know .items, .advantages .items");

    sliders.slick({
		slidesToShow: 1,
		slidesToScroll: 1,
        mobileFirst: true,
        arrows: true,
        infinite: false,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 992,
                settings: 'unslick'
            }
        ]
    });

    $(window).on('resize', function() {
        sliders.slick('resize');
    })

    // On before slide change
    sliders.on('beforeChange', function(event, slick, currentSlide, nextSlide){
        // Previous arrow.
        if (currentSlide > nextSlide) {
            $(this).find('.slick-next').removeClass('clicked');
            $(this).find('.slick-prev').addClass('clicked');
        }
        // Next arrow.
        if (currentSlide < nextSlide) {
            $(this).find('.slick-prev').removeClass('clicked');
            $(this).find('.slick-next').addClass('clicked');
        }
    });

    // On After slide change.
    sliders.on('afterChange', function(event, slick, currentSlide){
        $(this).find('.slick-prev').removeClass('clicked');
        $(this).find('.slick-next').removeClass('clicked');
    }); 
});
